package com.imaisnaini.kindeerlight.db.dao;

import com.imaisnaini.kindeerlight.db.model.Candle;

public class CandleDao extends BaseDaoCrud<Candle, Integer> {
    private static CandleDao candleDao;

    public static CandleDao getInstance(){
        if (candleDao == null){
            candleDao = new CandleDao();
        }
        return candleDao;
    }
}
