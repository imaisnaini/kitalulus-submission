package com.imaisnaini.kindeerlight.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Candle.TABLE_NAME)
public class Candle {
    public static final String TABLE_NAME = "candle";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String ICON = "icon";

    @DatabaseField(columnName = ID, id = true) private int id_candle;
    @DatabaseField(columnName = NAME) private String name;
    @DatabaseField(columnName = PRICE) private double price;
    @DatabaseField(columnName = ICON) private String icon;

    public Candle() {
    }

    public Candle(int id_candle, String name, double price, String icon) {
        this.id_candle = id_candle;
        this.name = name;
        this.price = price;
        this.icon = icon;
    }

    public int getId_candle() {
        return id_candle;
    }

    public void setId_candle(int id_candle) {
        this.id_candle = id_candle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
