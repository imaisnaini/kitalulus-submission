package com.imaisnaini.kindeerlight.view;

import com.imaisnaini.kindeerlight.db.model.Candle;

import java.util.List;

public interface CandleView {
    void showLoading();
    void hideLoading();
    void load(List<Candle> list);
}
