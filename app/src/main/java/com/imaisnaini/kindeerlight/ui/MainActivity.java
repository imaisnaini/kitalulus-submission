package com.imaisnaini.kindeerlight.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.imaisnaini.kindeerlight.R;
import com.imaisnaini.kindeerlight.db.dao.CandleDao;
import com.imaisnaini.kindeerlight.db.helper.Db;
import com.imaisnaini.kindeerlight.db.model.Candle;
import com.imaisnaini.kindeerlight.presenter.CandlePresenter;
import com.imaisnaini.kindeerlight.view.CandleView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements CandleView {
    @BindView(R.id.bottomNav_mainActivity)
    BottomNavigationView bottomNav;

    private boolean isFirstRun = false;
    private CandlePresenter mCandlePresenter;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Db.getInstance().init(this);
        initialCheck();
        initData();
        init();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()){
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_candles:
                    selectedFragment = new ProductFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_mainActivity,selectedFragment).commit();
            return true;
        }
    };

    // This method to initialaze view
    private void init() {
        Integer mode = getIntent().getIntExtra("mode", 0);

        bottomNav.setOnNavigationItemSelectedListener(navListener);

        if (mode == 0) {
            bottomNav.setSelectedItemId(R.id.nav_home);
        }else if (mode == 1){
            bottomNav.setSelectedItemId(R.id.nav_candles);
        }
    }

    private void initData(){
        mCandlePresenter = new CandlePresenter(this);

        if (isFirstRun){
            mCandlePresenter.initiateData();
        }
    }

    private void initialCheck(){
        try {
            List<Candle> candleList = CandleDao.getInstance().read();
            isFirstRun = (candleList.isEmpty() || false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("ISFIRSTRUN", String.valueOf(isFirstRun));
    }

    @Override
    public void showLoading() {
//        mDialog = new ProgressDialog(getApplicationContext());
//        mDialog.setMessage("Initiating data, please wait..");
//        mDialog.show();
    }

    @Override
    public void hideLoading() {
        //mDialog.dismiss();
    }

    @Override
    public void load(List<Candle> list) {

    }
}
