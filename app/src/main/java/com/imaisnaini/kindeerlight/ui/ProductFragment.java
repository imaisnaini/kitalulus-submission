package com.imaisnaini.kindeerlight.ui;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.imaisnaini.kindeerlight.R;
import com.imaisnaini.kindeerlight.adapter.CandleAdapter;
import com.imaisnaini.kindeerlight.db.model.Candle;
import com.imaisnaini.kindeerlight.presenter.CandlePresenter;
import com.imaisnaini.kindeerlight.view.CandleView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment implements CandleView {
    @BindView(R.id.rvContent_fragmentProduct)
    RecyclerView rvContent;
    @BindView(R.id.ivBack_fragmentProduct)
    ImageView ivBtnBack;

    private CandleAdapter mAdapter;
    private CandlePresenter mPresenter;
    private ProgressDialog mDialog;

    public ProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        init();
        initViews();
        return view;
    }

    private void init(){
        mPresenter = new CandlePresenter(this);
        mAdapter = new CandleAdapter(getActivity());
        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void initViews(){
    }

    @OnClick(R.id.ivBack_fragmentProduct) void onBackHome(){
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout_mainActivity, new HomeFragment())
                .commit();
    }
    @Override
    public void onResume() {
        super.onResume();
        mPresenter.load();
    }

    @Override
    public void showLoading() {
        mDialog = new ProgressDialog(getContext());
        mDialog.setMessage("Initiating data, please wait..");
        mDialog.show();
    }

    @Override
    public void hideLoading() {
        mDialog.hide();
    }

    @Override
    public void load(List<Candle> list) {
        mAdapter.generate(list);
        mAdapter.notifyDataSetChanged();
        rvContent.setAdapter(mAdapter);
    }
}
