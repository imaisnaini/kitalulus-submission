package com.imaisnaini.kindeerlight.presenter;

import androidx.cardview.widget.CardView;

import com.imaisnaini.kindeerlight.db.dao.CandleDao;
import com.imaisnaini.kindeerlight.db.model.Candle;
import com.imaisnaini.kindeerlight.view.CandleView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CandlePresenter {
    private CandleView mCandleView;

    public CandlePresenter(CandleView mCandleView) {
        this.mCandleView = mCandleView;
    }

    public void load(){
        mCandleView.showLoading();
        List<Candle> candleList = new ArrayList<>();
        try {
            candleList = CandleDao.getInstance().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mCandleView.load(candleList);
        mCandleView.hideLoading();
    }

    public void initiateData(){
        mCandleView.showLoading();

        List<Candle> candleList =  new ArrayList<>();
        candleList.add(new Candle(1, "Ataraxia Glass 60gr", 39000, "https://firebasestorage.googleapis.com/v0/b/kitalulus-kindeerlight.appspot.com/o/glass%20ataraxia%202.jpg?alt=media&token=056e45a9-60fa-49d4-aca1-a2014742d47f"));
        candleList.add(new Candle(2, "Elixir Glass 60gr", 39000, "https://firebasestorage.googleapis.com/v0/b/kitalulus-kindeerlight.appspot.com/o/glass%20elixir%203.jpg?alt=media&token=7cf5aa83-de81-4d97-ae5d-67e49d13c58e"));
        candleList.add(new Candle(3, "Halcyon Glass 60gr", 39000, "https://firebasestorage.googleapis.com/v0/b/kitalulus-kindeerlight.appspot.com/o/glass%20halcyon%203.jpg?alt=media&token=83fd04ec-0fc4-4c87-a1a0-19756a96b8ca"));
        candleList.add(new Candle(4, "Kefi Glass 60gr", 39000, "https://firebasestorage.googleapis.com/v0/b/kitalulus-kindeerlight.appspot.com/o/glass%20kefi%201.jpg?alt=media&token=da2f927e-b5c0-4610-a4aa-5b73316cc2a6"));

        for (Candle candle : candleList){
            try {
                CandleDao.getInstance().add(candle);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        mCandleView.hideLoading();
    }
}
