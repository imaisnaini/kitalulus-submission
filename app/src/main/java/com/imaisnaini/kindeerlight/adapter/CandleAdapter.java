package com.imaisnaini.kindeerlight.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imaisnaini.kindeerlight.R;
import com.imaisnaini.kindeerlight.db.model.Candle;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CandleAdapter extends RecyclerView.Adapter<CandleAdapter.ViewHolder> {
    private Context mContext;
    private List<Candle> mList = new ArrayList<>();

    public CandleAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public CandleAdapter(Context mContext, List<Candle> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rvcontent, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Candle candle = mList.get(position);
        holder.tvName.setText(candle.getName());
        holder.tvPrice.setText(String.valueOf(candle.getPrice()));
        Picasso.get().load(candle.getIcon()).into(holder.ivIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvProductName_itemRvContent)
        TextView tvName;
        @BindView(R.id.tvPrice_itemRvContent)
        TextView tvPrice;
        @BindView(R.id.ivIcon_itemRvContent)
        ImageView ivIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void generate(List<Candle> list){
        clear();
        this.mList = list;
        notifyDataSetChanged();
    }

    public void clear(){
        this.mList.clear();
        notifyDataSetChanged();
    }
}
